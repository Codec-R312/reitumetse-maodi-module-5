Instructions:

Set up a Firebase account.
Create a Gitlab account.
Create a repository on Gitlab as name-surname-module-5


Assessment:
All the code must be submitted through a Gitlab repository link. 

Create a project on Firebase
Connect your web app to Firebase
Add a form to your app with at least 3 input fields
Write code to connect your app to the database to create, read, update and delete
